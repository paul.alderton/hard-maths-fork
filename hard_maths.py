def adding(a: float, b: float) -> float:
    return a + b

def multiplication(a: float, b: float) -> float:
    return a * b

if __name__ == "__main__":
    a = 10
    b = 20
    print(f"{a} + {b} = {adding(a, b)}!")
    print(f"{a} * {b} = {multiplication(a, b)}!")